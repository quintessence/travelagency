package edu.khai.practice.travelagency.controller;

import edu.khai.practice.travelagency.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class MailController {

  private UserService userService;

  public MailController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("login")
  public String showLoginPage() {
    return "login";
  }

  @PostMapping("login")
  public String authorize(@RequestParam String email, @RequestParam String password) {
    if (userService.authorize(email, password)) {
      return "redirect:/tours";
    }
    return "redirect:/login";
  }

  @GetMapping("tours")
  public String showToursPage() {
    return "tours";
  }
}
