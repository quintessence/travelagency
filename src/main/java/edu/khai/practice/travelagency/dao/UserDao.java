package edu.khai.practice.travelagency.dao;

import edu.khai.practice.travelagency.entity.User;

public interface UserDao {

  User getUser(String email);
}
