package edu.khai.practice.travelagency.service.impl;

import edu.khai.practice.travelagency.dao.UserDao;
import edu.khai.practice.travelagency.entity.User;
import edu.khai.practice.travelagency.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

  private UserDao userDao;

  public UserServiceImpl(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public boolean authorize(String email, String password) {
    User user = userDao.getUser(email);
    if (user != null) {
      LOGGER.trace("Attempt to login as " + user.getEmail());
      return StringUtils.equals(password, user.getPassword());
    }
    return false;
  }
}
