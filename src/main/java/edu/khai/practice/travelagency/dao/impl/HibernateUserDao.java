package edu.khai.practice.travelagency.dao.impl;

import edu.khai.practice.travelagency.dao.UserDao;
import edu.khai.practice.travelagency.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateUserDao implements UserDao {

  private SessionFactory sessionFactory;

  public HibernateUserDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public User getUser(String email) {
    User user;
    try (Session session = sessionFactory.openSession()) {
      session.beginTransaction();
      user = (User) session
          .createQuery(String.format("from User u where u.email='%s'", email))
          .uniqueResult();
      session.getTransaction().commit();
    }
    return user;
  }
}
